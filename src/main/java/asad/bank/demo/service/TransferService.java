package asad.bank.demo.service;

import asad.bank.demo.dto.TransferDto;

public interface TransferService {
    String tranferAccountToAccount(TransferDto transferDto);
}
