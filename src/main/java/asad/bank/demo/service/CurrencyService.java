package asad.bank.demo.service;

import asad.bank.demo.domain.Currency;
import asad.bank.demo.dto.CurrencyDto;

public interface CurrencyService {
    Currency insertNewType(CurrencyDto currencyDto);
}
