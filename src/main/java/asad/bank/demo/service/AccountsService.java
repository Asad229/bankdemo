package asad.bank.demo.service;

import asad.bank.demo.domain.Accounts;
import asad.bank.demo.dto.AccountDto;
import asad.bank.demo.dto.AccountsResponseDto;

import java.util.List;

public interface AccountsService {
    List<AccountsResponseDto> getAccountsByUserName();
    Accounts createAccount(AccountDto accountDto);
}
