package asad.bank.demo.service.impl;

import asad.bank.demo.repository.AccountsRepository;
import asad.bank.demo.domain.Accounts;
import asad.bank.demo.domain.User;
import asad.bank.demo.dto.AccountDto;
import asad.bank.demo.dto.AccountsResponseDto;
import asad.bank.demo.service.AccountsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountsServiceImpl implements AccountsService {
    private final AccountsRepository accountsRepository;
    private final ModelMapper modelMapper;

    @Override
    public List<AccountsResponseDto> getAccountsByUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        var principal = auth.getPrincipal();
        var user = modelMapper.map(principal, User.class);
        var accounts = accountsRepository.findAllByUserName(user.getUsername())
                .stream()
                .filter(f -> f.getActive() == true)
                .map(a -> modelMapper.map(a, AccountsResponseDto.class))
                .collect(Collectors.toList());
        log.info("User {} 's accounts {}", user.getUsername(), accounts);
        return accounts;
    }

    @Override
    public Accounts createAccount(AccountDto accountDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        var principal = auth.getPrincipal();
        var user = modelMapper.map(principal, User.class);
        var account = Accounts.builder()
                .accountNo(UUID.randomUUID().toString())
                .active(true)
                .amount(BigDecimal.ZERO)
                .currency(accountDto.getCurrency())
                .user(user).build();
        log.info("Account create {}, user {}", account, user);
        accountsRepository.save(account);
        return account;
    }
}
