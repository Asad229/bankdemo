package asad.bank.demo.service.impl;

import asad.bank.demo.repository.AccountsRepository;
import asad.bank.demo.repository.CurrencyRepository;
import asad.bank.demo.domain.Accounts;
import asad.bank.demo.domain.CurrencyType;
import asad.bank.demo.domain.User;
import asad.bank.demo.dto.TransferDto;
import asad.bank.demo.exception.AccountNotFoundException;
import asad.bank.demo.exception.AccountStatusException;
import asad.bank.demo.exception.InsufficentBalanceException;
import asad.bank.demo.exception.SameAccountException;
import asad.bank.demo.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransferServiceImpl implements TransferService {
    private final AccountsRepository accountsRepository;
    private final ModelMapper modelMapper;
    private final CurrencyRepository currencyRepository;

    @Transactional
    @Override
    public String tranferAccountToAccount(TransferDto transferDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        var principal = auth.getPrincipal();
        var user = modelMapper.map(principal, User.class);
        var senderDetails = accountsRepository.findByAccountNo(transferDto.getSender())
                .orElseThrow(AccountNotFoundException::new);
        var receiverDetails = accountsRepository.findByAccountNo(transferDto.getReceiver())
                .orElseThrow(AccountNotFoundException::new);
        balanceChecker(senderDetails, transferDto);
        accountChecker(transferDto);
        statusChecker(senderDetails, receiverDetails);
        var newBalanceSender = calculateSenderAmount(senderDetails.getAmount(), transferDto.getAmount());
        if (senderDetails.getCurrency().equals(receiverDetails.getCurrency())) {
            var newBalanceReceiver = calculateReceiverAmount(receiverDetails.getAmount(), transferDto.getAmount());
            var receiverResp = accountsRepository.updateBalanceReceiver(receiverDetails.getAccountNo(), newBalanceReceiver);
            log.info("Receiver balance was updated {} response {}", newBalanceReceiver, receiverResp);
        } else {
            var newBalanceReceiver = calculateReceiverAmountCurrency(receiverDetails.getAmount(), transferDto.getAmount(),
                    senderDetails.getCurrency(), receiverDetails.getCurrency());
            var receiverResp = accountsRepository.updateBalanceReceiver(receiverDetails.getAccountNo(), newBalanceReceiver);
            log.info("Receiver balance was updated {} response {}", newBalanceReceiver, receiverResp);
        }
        var senderResp = accountsRepository.updateBalanceSender(senderDetails.getAccountNo(), user.getId(), newBalanceSender);
        log.info("Sender balance was updated {} response {}", newBalanceSender, senderResp);
        return "SUCESS";
    }

    private void balanceChecker(Accounts senderDetails, TransferDto transferDto) {
        if (senderDetails.getAmount() == null
                || senderDetails.getAmount().compareTo(BigDecimal.ZERO) < 1
                || senderDetails.getAmount().compareTo(transferDto.getAmount()) < 0) {
            log.error("Customer has not enough amount!");
            throw new InsufficentBalanceException();
        }
    }

    private void accountChecker(TransferDto transferDto) {
        if (transferDto.getSender().equalsIgnoreCase(transferDto.getReceiver())) {
            throw new SameAccountException();
        }
    }

    private void statusChecker(Accounts senderDetails, Accounts receiverDetails) {
        if (!senderDetails.getActive() || !receiverDetails.getActive()) {
            log.error("Account is not active!");
            throw new AccountStatusException();
        }
    }

    private BigDecimal calculateSenderAmount(BigDecimal userBalance, BigDecimal transferAmount) {
        return userBalance.subtract(transferAmount);
    }

    private BigDecimal calculateReceiverAmount(BigDecimal userBalance, BigDecimal transferAmount) {
        return userBalance.add(transferAmount);
    }

    private BigDecimal calculateReceiverAmountCurrency(BigDecimal userBalance, BigDecimal transferAmount,
                                                       CurrencyType currencyType, CurrencyType exchangedCurrencyType) {

        var currencyDto = currencyRepository.getCurrencyDto(currencyType.getId(), exchangedCurrencyType.getId());
        if (currencyType.equals(currencyDto.getCurrency())) {
            return userBalance.add(transferAmount.divide(currencyDto.getValue(), 2, RoundingMode.FLOOR));
        } else {
            return userBalance.add(transferAmount.multiply(currencyDto.getValue()));
        }
    }
}
