package asad.bank.demo.service.impl;

import asad.bank.demo.repository.CurrencyRepository;
import asad.bank.demo.domain.Currency;
import asad.bank.demo.dto.CurrencyDto;
import asad.bank.demo.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyRepository currencyRepository;

    @Override
    public Currency insertNewType(CurrencyDto currencyDto) {
        var newCurrency = Currency.builder()
                .currency(currencyDto.getCurrency())
                .exchangedCurrency(currencyDto.getExchangedCurrency())
                .value(currencyDto.getValue())
                .build();
        log.info("New currency details {} is inserting...", newCurrency);
        currencyRepository.save(newCurrency);
        return newCurrency;
    }

}
