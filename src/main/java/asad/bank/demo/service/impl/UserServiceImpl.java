package asad.bank.demo.service.impl;

import asad.bank.demo.dto.request.LoginRequest;
import asad.bank.demo.dto.response.RegisterDto;
import asad.bank.demo.repository.UserRepository;
import asad.bank.demo.domain.User;
import asad.bank.demo.dto.UserDTO;
import asad.bank.demo.auth.JwtUtils;
import asad.bank.demo.dto.response.LoginResponse;
import asad.bank.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final JwtUtils jwtUtils;
    private final PasswordEncoder encoder;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    @Override
    public ResponseEntity<LoginResponse> loginUser(LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new LoginResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                roles));
    }

    @Override
    public ResponseEntity<RegisterDto> registrationUser(UserDTO dto) {
        if (userRepository.existsByUsername(dto.getUsername())) {
            return ResponseEntity.badRequest().body(
                    RegisterDto.builder()
                    .message("Error: Username is already taken!")
                    .build());
        }

        User user = new User(dto.getId(), dto.getUsername(), dto.getUsername(),
                encoder.encode(dto.getPassword()),null,"USER");
        userRepository.save(user);
        return ResponseEntity.ok().body(
                RegisterDto.builder()
                        .message("User registered successfully!")
                        .build());
    }

}
