package asad.bank.demo.service;

import asad.bank.demo.dto.request.LoginRequest;
import asad.bank.demo.dto.response.RegisterDto;
import asad.bank.demo.dto.UserDTO;
import asad.bank.demo.dto.response.LoginResponse;
import org.springframework.http.ResponseEntity;


public interface UserService {
     ResponseEntity<LoginResponse> loginUser(LoginRequest loginRequest);
     ResponseEntity<RegisterDto> registrationUser(UserDTO dto);
}
