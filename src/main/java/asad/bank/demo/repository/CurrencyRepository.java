package asad.bank.demo.repository;

import asad.bank.demo.domain.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {
    @Query(nativeQuery = true, value = "select * from currency where (currency=:currencyType and exchanged_currency=:exchangedCurrencyType)" +
            "or  (currency=:exchangedCurrencyType and exchanged_currency=:currencyType) limit 1")
    Currency getCurrencyDto(int currencyType, int exchangedCurrencyType);

    @Modifying
    @Query(nativeQuery = true, value = "update currency set value=:rate where currency=:currencyType and exchanged_currency=:exchangedCurrencyType")
    int updateCurrencyValues(BigDecimal rate, int currencyType, int exchangedCurrencyType);

    @Override
    List<Currency> findAll();
}
