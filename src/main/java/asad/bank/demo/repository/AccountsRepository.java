package asad.bank.demo.repository;

import asad.bank.demo.domain.Accounts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface AccountsRepository extends JpaRepository<Accounts, Long> {

    @Query(nativeQuery = true, value = "select a.* from accounts a join (select id from user where username=:username) u on a.user_id=u.id")
    List<Accounts> findAllByUserName(String username);

    @Query(nativeQuery = true, value = "select * from accounts where account_no=:accountNo")
    Optional<Accounts> findByAccountNo(String accountNo);

    @Modifying
    @Query(nativeQuery = true, value = "update accounts set amount=:newAmount where user_id=:userId and account_no=:accountNo")
    int updateBalanceSender(String accountNo, Long userId, BigDecimal newAmount);

    @Modifying
    @Query(nativeQuery = true, value = "update accounts set amount=:newAmount where account_no=:accountNo")
    int updateBalanceReceiver(String accountNo, BigDecimal newAmount);
}
