package asad.bank.demo.dto;

import asad.bank.demo.domain.CurrencyType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
public class CurrencyDto {
    @NotBlank
    private CurrencyType currency;
    @NotBlank
    private CurrencyType exchangedCurrency;
    @NotBlank
    private BigDecimal value;
}
