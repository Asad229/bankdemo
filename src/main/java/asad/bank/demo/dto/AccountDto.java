package asad.bank.demo.dto;

import asad.bank.demo.domain.CurrencyType;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AccountDto {
    @NotBlank
    private CurrencyType currency;
}
