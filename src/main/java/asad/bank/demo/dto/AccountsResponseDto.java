package asad.bank.demo.dto;

import asad.bank.demo.domain.CurrencyType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountsResponseDto {
    private String accountNo;
    private CurrencyType currency;
    private BigDecimal amount;

}
