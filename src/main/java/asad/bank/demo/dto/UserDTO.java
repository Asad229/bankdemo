package asad.bank.demo.dto;

import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserDTO {
    private Long id;
    @NotBlank
    @Size(min = 4, max = 20,message = "Username's size must be between 4 and 20")
    private String username;
    @NotBlank
    @Size(min = 4, max = 40,message = "Password's size must be between 4 and 40")
    private String password;

}
