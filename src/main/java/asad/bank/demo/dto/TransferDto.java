package asad.bank.demo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
public class TransferDto {
    @NotBlank
    private String sender;
    @NotBlank
    private String receiver;
    @NotBlank
    @Positive
    private BigDecimal amount;
}
