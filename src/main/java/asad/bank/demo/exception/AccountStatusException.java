package asad.bank.demo.exception;


public class AccountStatusException extends RuntimeException{


    private static final long serialVersionUID = -3042686055658047285L;

    public AccountStatusException() {
        super("Account is not active.");
    }

    public AccountStatusException(String account) {
        super(String.format("Current account %d is not active.", account));
    }
}
