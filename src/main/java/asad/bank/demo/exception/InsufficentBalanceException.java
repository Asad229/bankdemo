package asad.bank.demo.exception;

import java.math.BigDecimal;

public class InsufficentBalanceException extends RuntimeException{


    private static final long serialVersionUID = -3042686055658047285L;

    public InsufficentBalanceException() {
        super("Balance not enough.");
    }

    public InsufficentBalanceException(BigDecimal currentBalance) {
        super(String.format("Current balance is %d but you want to send %s.", currentBalance));
    }
}
