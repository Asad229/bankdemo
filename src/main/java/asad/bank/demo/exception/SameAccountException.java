package asad.bank.demo.exception;

public class SameAccountException extends RuntimeException{


    private static final long serialVersionUID = -3042686055658047285L;

    public SameAccountException() {
        super("Sender and receiver accounts are same.");
    }

    public SameAccountException(String account) {
        super(String.format("Account are same %s.", account));
    }
}
