package asad.bank.demo.exception;


public class AccountNotFoundException extends RuntimeException{


    private static final long serialVersionUID = -3042686055658047285L;

    public AccountNotFoundException() {
        super("Account not found.");
    }

    public AccountNotFoundException(String account) {
        super(String.format("Account %d not found.", account));
    }
}
