package asad.bank.demo.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CurrencyType {
    AZN(0,"AZN"),
    USD(1,"USD"),
    EUR(2,"EUR");
    private final int id;
    private final String currency;

}
