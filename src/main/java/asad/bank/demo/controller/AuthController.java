package asad.bank.demo.controller;

import asad.bank.demo.dto.UserDTO;
import asad.bank.demo.dto.request.LoginRequest;
import asad.bank.demo.service.UserService;
import asad.bank.demo.dto.response.RegistrDto;
import asad.bank.demo.dto.response.LoginResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;

    @PostMapping("/signin")
    public ResponseEntity<LoginResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
       return userService.loginUser(loginRequest);
    }

    @PostMapping("/signup")
    public ResponseEntity<RegistrDto> registerUser(@Valid @RequestBody UserDTO userDTO) {
        return userService.registrationUser(userDTO);
    }

}