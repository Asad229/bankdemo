package asad.bank.demo.controller;

import asad.bank.demo.dto.TransferDto;
import asad.bank.demo.service.impl.TransferServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/transfer")
public class TransferController {
    private final TransferServiceImpl transferService;

    @PostMapping("/account2account")
    public String transferAccountToAccount(@RequestBody @Valid TransferDto transferDto) {
        log.info("Transfer details is: {}", transferDto);
        return transferService.tranferAccountToAccount(transferDto);
    }
}
