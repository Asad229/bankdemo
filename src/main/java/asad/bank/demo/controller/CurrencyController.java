package asad.bank.demo.controller;

import asad.bank.demo.dto.CurrencyDto;
import asad.bank.demo.service.impl.CurrencyServiceImpl;
import asad.bank.demo.domain.Currency;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/currency")
public class CurrencyController {
    private final CurrencyServiceImpl currencyService;

    @PostMapping
    public Currency insertCurrency(@RequestBody @Valid CurrencyDto currencyDto){
        var currency = currencyService.insertNewType(currencyDto);
        log.info("New currency was created {}",currency);
        return currency;
    }
}
