package asad.bank.demo.controller;

import asad.bank.demo.repository.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Component
@EnableScheduling
@RequiredArgsConstructor
public class CurrencyScheduler {
    private final CurrencyRepository currencyRepository;
    public static final List<BigDecimal> ALLOWED_RATES = List.of(new BigDecimal(0.1));
    public static final List<String> ALLOWED_CURRENCY = List.of("AZN", "USD");


    @Scheduled(fixedRate = 60 * 1000)
    @Transactional
    public void checkSchedules() {
        log.info("Schedules is running...");
        var currencies = currencyRepository.findAll();
        currencies.stream()
                .forEach(z -> ALLOWED_RATES
                        .stream()
                        .forEach(i -> currencyRepository
                                .updateCurrencyValues(i.add(z.getValue()), z.getCurrency().getId(), z.getExchangedCurrency().getId())));
    }
}
