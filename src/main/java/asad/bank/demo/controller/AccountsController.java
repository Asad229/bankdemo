package asad.bank.demo.controller;

import asad.bank.demo.domain.Accounts;
import asad.bank.demo.dto.AccountDto;
import asad.bank.demo.dto.AccountsResponseDto;
import asad.bank.demo.service.impl.AccountsServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/account")
public class AccountsController {
    private final AccountsServiceImpl accountsService;

    @GetMapping("/accounts")
    private List<AccountsResponseDto> getUserAccounts() {
        return accountsService.getAccountsByUserName();
    }

    @PostMapping
    private Accounts createAccount(@RequestBody @Valid AccountDto accountDto) {
        return accountsService.createAccount(accountDto);
    }


}
