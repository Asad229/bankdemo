package asad.bank.demo.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;

@UtilityClass
public final class TestConstants {
    public static final String PATH_GET_DETAILED_REWARD = "/coupons/{id}";
    public static final String PATH_AUTH_SIGNIN= "/auth/signin";

    public static final String CLASSPATH_PREFIX = "classpath:";
    public static final String PATH_MOCK = CLASSPATH_PREFIX + "mock/";
    public static final String PATH_MOCK_REWARDS_LIST = PATH_MOCK + "accounts.json";

    public static final ObjectMapper DEFAULT_OBJECT_MAPPER = TestHelper.objectMapper();

}
