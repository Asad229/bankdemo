package asad.bank.demo.utils;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;

public class TestConfiguration {
    public static final EasyRandomParameters RANDOM_PARAMETERS = new EasyRandomParameters()
            .seed(123L)
            .objectPoolSize(100)
            .randomizationDepth(1)
            .charset(StandardCharsets.UTF_8)
            .timeRange(LocalTime.of(9, 0), LocalTime.of(18, 0))
            .dateRange(LocalDate.now(), LocalDate.now().plusDays(10))
            .stringLengthRange(5, 50)
            .collectionSizeRange(1, 10)
            .scanClasspathForConcreteTypes(true)
            .overrideDefaultInitialization(false)
            .ignoreRandomizationErrors(true);

    public static final EasyRandom EASY_RANDOM = new EasyRandom(RANDOM_PARAMETERS);
}
