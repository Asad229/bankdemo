package asad.bank.demo.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;

public interface TestHelper {
    static <T> T loadFromJson(String path, Class<T> clazz) throws IOException {
        InputStream in = loadFile(path);
        return TestConstants.DEFAULT_OBJECT_MAPPER.readValue(in, clazz);
    }

    static InputStream loadFile(String fileName) {
        if (fileName.startsWith(TestConstants.CLASSPATH_PREFIX)) {
            String newFileName = fileName.substring(TestConstants.CLASSPATH_PREFIX.length());
            return Thread.currentThread().getContextClassLoader().getResourceAsStream(newFileName);
        }
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
    }

    static String loadFileAsString(String fileName) throws IOException {
        InputStream in = loadFile(fileName);
        return streamToString(in);
    }

    static String streamToString(InputStream inputStream) throws IOException {
        return new String(FileCopyUtils.copyToByteArray(inputStream), UTF_8);
    }

    static ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper;
    }
}
