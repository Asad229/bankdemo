READ ME

UniTech Project

1. terminal -> docker-compose up or use mysql (for details application.yaml)

2. default port 8081 and context-path is /api

3. To registr
POST -> {{url}}/api/auth/signup
{
	"username":"testuser",
	"password":"testpass"
}

4. To login
POST -> {{url}}/api/auth/signin
{
	"username":"testuser",
	"password":"testpass"
}
Get token and use as BearerToken.

5. Insert currency types (not need authorization)
When you start app, schedular will start. Currency value will change every minute.
POST -> {{url}}/api/currency
{
    "currency":"AZN",
    "exchangedCurrency":"USD",
    "value":1.7
}

6. Create account
POST -> {{url}}/api/account
{
    "currency":"AZN"
}
And get account details

7. Get all accounts
GET -> {{url}}/api/account/accounts

8.  Do a2a tranfer
POST -> {{url}}/api/transfer/account2account
{
    "sender": "a1795ede-64ee-43ba-902f-3e16dc006fdb",
    "receiver": "ed14e7cf-188a-417f-b815-1ff7e858be40",
    "amount": 1
}

Thanks!
